# elk-kafka-parent

#### 介绍
ELK+Kafka搭建分布式日志采集环境用来测试效果的代码端

#### 软件架构
| 技术       | 版本          |
| ---------- | ------------- |
| SpringBoot | 2.1.5.RELEASE |
| Kafka      | 2.2.6.RELEASE |
| Lombok     | 1.18.8        |
| FastJson   | 1.2.47        |

elk-kafka-parent----elk-kafka-common（AOP日志管理、全局异常处理、kafka发布消息）
                ----elk-kafka-order（引入common模块、整合kafka服务、service自检异常测试效果）

#### 安装教程

1. 搭建好ELK服务环境；
2. 搭建好Kafka服务环境；
3. springboot整合kafka.

#### 使用说明

1. springboot新建测试用的entity、service、controller；
2. 引入AOP管理日志信息，前置通知推送request信息到kafka，后置通知推送response信息到kafka;
3. 新增全局异常处理，推送异常日志信息到kafka;
4. 在order服务中自建一个异常，如zero异常、空指针异常等等，测试是否会发布异常到kafka，logstash是否会订阅kafka中的topic:order_log，日志是否会推送到es中去，kibana中是否能查看到es中的日志信息。


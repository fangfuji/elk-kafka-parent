package com.guoj.elk.kafka.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @作者: guoj
 * @日期: 2019/6/10 15:39
 * @描述:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity implements Serializable {

	private String orderId;

	private String orderName;

	private String orderTime;

}

package com.guoj.elk.kafka.service;

import com.guoj.elk.kafka.entity.OrderEntity;

/**
 * @作者: guoj
 * @日期: 2019/6/10 15:36
 * @描述:
 */
public interface OrderService {
	public boolean sendOrder(OrderEntity orderEntity);
}

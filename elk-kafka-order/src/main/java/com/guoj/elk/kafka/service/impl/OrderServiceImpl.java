package com.guoj.elk.kafka.service.impl;

import com.guoj.elk.kafka.entity.OrderEntity;
import com.guoj.elk.kafka.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @作者: guoj
 * @日期: 2019/6/10 15:37
 * @描述:
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService{

	@Override
	public boolean sendOrder(OrderEntity orderEntity) {

		// 测试by zero异常
//		 int i = 1/0;

		// 测试空指针异常
		 String user = null;
		 System.out.println(user.getBytes());

		log.info("==================订单信息===================");
		log.info("订单号：" + orderEntity.getOrderId());
		log.info("订单名称：" + orderEntity.getOrderName());
		log.info("订单时间：" + orderEntity.getOrderTime());
		log.info("==================订单信息===================");

		return true;
	}

}

package com.guoj.elk.kafka.controller;

import com.guoj.elk.kafka.entity.OrderEntity;
import com.guoj.elk.kafka.entity.ResponseEntity;
import com.guoj.elk.kafka.entity.StatusCode;
import com.guoj.elk.kafka.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @作者: guoj
 * @日期: 2019/6/10 15:36
 * @描述:
 */
@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@PostMapping("/sendOrder")
	public ResponseEntity sendOrder(@RequestBody OrderEntity orderEntity) {
		boolean result = orderService.sendOrder(orderEntity);
		if (!result) {
			return new ResponseEntity(false, StatusCode.ERROR, "发送订单失败");
		}
		return new ResponseEntity(true, StatusCode.OK, "发送订单成功", orderEntity);
	}
	
}

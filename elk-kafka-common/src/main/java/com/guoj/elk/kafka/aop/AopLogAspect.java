package com.guoj.elk.kafka.aop;

import com.alibaba.fastjson.JSONObject;
import com.guoj.elk.kafka.kafka.KafkaSender;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * 
 * @description: ELK拦截日志信息
 * -----
 * 		多个项目 每个项目对应不同的主题 不同的主题对应不同的Logstash
 *		请求与响应的日志 是如何区分呢？ 请求日志: {"request":{}} 响应日志：{"response":{}} 错误日志：{"error":{}}
 *		kibana中直接查询error
 *
 */
@Aspect
@Component
@Slf4j
public class AopLogAspect {
	@Autowired
	private KafkaSender<JSONObject> kafkaSender;

	// 申明一个切点 里面是 execution表达式
	@Pointcut("execution(* com.guoj.*.*.service.impl.*.*(..))")
	private void serviceAspect() {
	}

	// 请求method前打印内容
	@Before(value = "serviceAspect()")
	public void methodBefore(JoinPoint joinPoint) {
		System.out.println("前置通知...");
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = requestAttributes.getRequest();

		// // 打印请求内容
		// log.info("===============请求内容===============");
		// log.info("请求地址:" + request.getRequestURL().toString());
		// log.info("请求方式:" + request.getMethod());
		// log.info("请求类方法:" + joinPoint.getSignature());
		// log.info("请求类方法参数:" + Arrays.toString(joinPoint.getArgs()));
		// log.info("===============请求内容===============");

		JSONObject jsonObject = new JSONObject();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		// ip
		jsonObject.put("request_ip", request.getRemoteAddr());
		// 主机名
		jsonObject.put("request_host", request.getRemoteHost());
		// 端口号
		jsonObject.put("request_port", request.getRemotePort());
		// 请求时间
		jsonObject.put("request_time", df.format(new Date()));
		// 请求URL
		jsonObject.put("request_url", request.getRequestURL().toString());
		// 请求的方法
		jsonObject.put("request_method", request.getMethod());
		// 请求类方法
		jsonObject.put("signature", joinPoint.getSignature());
		// 请求参数
		jsonObject.put("request_args", Arrays.toString(joinPoint.getArgs()));

		JSONObject requestJsonObject = new JSONObject();
		requestJsonObject.put("request", jsonObject);

		log.info("请求-requestJsonObject: " + requestJsonObject);

		kafkaSender.send(requestJsonObject);
	}

	// 在方法执行完结后打印返回内容
	@AfterReturning(returning = "o", pointcut = "serviceAspect()")
	public void methodAfterReturing(Object o) {
		System.out.println("后置通知...");
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = requestAttributes.getRequest();
		// log.info("--------------返回内容----------------");
		// log.info("Response内容:" + gson.toJson(o));
		// log.info("--------------返回内容----------------");
		JSONObject respJSONObject = new JSONObject();
		JSONObject jsonObject = new JSONObject();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		// ip
		jsonObject.put("response_ip", request.getRemoteAddr());
		// 主机名
		jsonObject.put("response_host", request.getRemoteHost());
		// 端口号
		jsonObject.put("response_port", request.getRemotePort());
		// 响应时间
		jsonObject.put("response_time", df.format(new Date()));
		// 响应内容
		jsonObject.put("response_content", JSONObject.toJSONString(o));

		respJSONObject.put("response", jsonObject);

		log.info("响应-responseJSONObject: " + respJSONObject);

		kafkaSender.send(respJSONObject);

	}

}
